$('.fade-slider').slick({
  dots: false,
  infinite: true,
  speed: 300,
  fade: true,
  cssEase: 'linear'
});

$('.responsive-slider').slick({
  dots: false,
  infinite: true,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 576,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});


$('.three-slider').slick({
  dots: false,
  infinite: true,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1365,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
      }
    },
    {
      breakpoint: 576,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});



$('.eight-slider').slick({
  dots: false,
  infinite: true,
  speed: 300,
  slidesToShow: 9,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 7,
        slidesToScroll: 1
      }
    },

    {
      breakpoint: 992,
      settings: {
        slidesToShow: 6,
        slidesToScroll: 1
      }
    },

    {
      breakpoint: 576,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

$(".mobile_bars").on("click", function () {
  $(".mobile_menu").addClass("active")
  $("body").css("overflow-y", "hidden")
})


$(".mobile_times").on("click", function () {
  $(".mobile_menu").removeClass("active")
  $("body").css("overflow-y", "unset")
})


$(".mobile_right_menu").on("click", function () {
  $(".mobile_menu").removeClass("active")
  $("body").css("overflow-y", "unset")
})



$('.slider-product').slick({
  slidesToShow: 1,
  // slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.slider-product-nav'
});
$('.slider-product-nav').slick({
  slidesToShow: 5,
  slidesToScroll: 1,
  asNavFor: '.slider-product',
  dots: false,
  centerMode: true,
  focusOnSelect: true,
  arrows: false,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 3,  
        slidesToScroll: 1
      }
    }
  ]
});
